# CS Games 2020
## Présentation 
![](img/AGEEI_logo2015.png)
Corinne Pulgar - VP Compétition

## Bureau des compétitions
<p><img src="img/BDClogoNoirBlanc.png" height="700" /></p>

# CS Games

## Selon le site officiel
> Les Jeux CS sont une compétition interuniversitaire regroupant des épreuves touchant différents domaines de l'informatique.

##
> Des problèmes de logique et d'algorithmes, en passant par des jeux vidéos, des activités à caractère social et, certainement, 
des défis de programmation y sont tous présentés au cours d'une fin de semaine.
[csgames.org](https://csgames.org/)


# Concrètement, c'est...
## 3 jours de compétitions 
![](photo-cs2019/competition.jpg)

## 3 jours de party
![](photo-cs2019/party.jpg)

## Une trentaine défis
- Base de données
- Extreme programming
- Hardware
- Web
- et plus à venir

## En vrai, ça ressemble à quoi
+ Prog
+ Flashout
+ Party jusqu'à ...
+ Réveille avant 8h
+ Prog
+ Party jusqu'à ...
+ Réveille avant 8h
+ Prog
+ Banquet chic (vin à volonté)
+ Encore plus gros party!
+ Dort toute la journée


# Précédemment aux CS games
##
<p float="left">
  <img src="img/CS19_logo.png" height="350" />
  <img src="img/CS18_logo.png" height="350" />
  <img src="img/CS17_logo.png" height="350" /> 
  <img src="img/CS16_logo.png" height="350" />
</p>

## Depuis combien de temps?
![](img/historique.png)</br>
2003</br>
et aucune victoire... encore!

## Combien d'université?
Plein... Genre plus que 10!

# Objectif
## Gagner des petits prix
![](photo-cs2019/petits.jpg)

## ... des petits trophées
![](photo-cs2019/gros.jpg)

## ... et un GROS!
![](photo-cs2019/win_ets.jpg)

# Photos des CS Games 2018
##
![](photo-cs2019/mirego.jpg)

##
![](photo-cs2019/sports.jpg)

##
![](photo-cs2019/gala_uqam.jpg)

# Le thème de cette année
##
![](img/CS20_logo.png){height=600}

Organisé par l'ETS

[CS Games 2019](http://2019.csgames.org)

## Lore
> **For the last few years, technology has been evolving at an exponential rate. We are no
longer safe today.** </br>
This rapid technological evolution has radically disrupted our way of
thinking, our behaviour and our means of communication. Artificial intelligence, data
theft, deep fakes, extreme surveillance, and espionage have all invaded our lives. They
are increasingly worrying the populace.

##
> To resolve this crisis, the governments of the world have banded together to impose a
moratorium on technological development of any kind. They have begun encouraging
a return to the old ways and actively prohibit the use of modern technology. It has been
several months since the new regulations have been adopted unanimously under the
name: **Technological Prohibition**. 

## 
> Believing that we should give technology a second chance, a group named
**UndergradSociety** has established an insurgency movement. Seeking to reverse the
recent decisions made by the governments, this group will show humanity that
technology does more good than bad for our modern society.
To do so, they have decided to assemble the best technophiles from each university in
order to counter these governmental actions. No matter your specialty,
UndergradSociety needs you! 

# Compétences requises
## Aimer la prog et être motivé <3
</br>
</br>
Pour vrai c'est tout.

## Qualifications
Ouverture le 8 janvier 00:00:02

Fermeture le 22 janvier 23:59:58

## D'habitude

Il y a deux équipes, une de noobs et une de pro...

<p float="left">
  <img src="img/WAVsurfersHD.png" height="350" />
  <img src="img/XargonautsHD.png" height="350" />
</p>

## Cette année

Une équipe complètement féminine! </br>

Rencontre le 3 décembre à 12h45 ici-même : </br>
- Rencontre avec les participantes de l'année dernière </br>
- Dévoilement du sous-thème de cette année

## En attendant
Slack : #competitions

@corinnep et @Mélanie

www.ctf.ageei.uqam.ca

## Viens donc, ça va être nice!

![](photo-cs2019/fun.jpg)
