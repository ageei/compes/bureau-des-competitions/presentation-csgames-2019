# CS Games 2020
Édition paritaire 

##
<p><img src="img/BDClogoNoirBlanc.png" height="600" /></p>
Corinne Pulgar - VP Compétition

# L'épiphanie
![](photo-cs2019/gala_uqam.jpg)

## 
![](photo-cs2019/femmes.jpg)

## Les femmes en sciences
<p>
  <img src="img/stem.jpg" height="300"/>
  <img src="img/IT.jpg" height="300"/>
</p>

## Historique
![](img/composition.png)

## Initiative passée
En 2016, l'UQÀM accueille les CSGames pour la première fois
<p><img src="img/CS16_logo.png" height="400" /></p>
et annonce que la participation pour les équipes entièrement féminine sera **gratuite**.

##
Alexandre Terrasa, vice-président du comité organisateur

> Pour la deuxième fois cette année, une équipe entièrement féminine prendra part aux CS Games, mentionne fièrement le président du comité organisateur. C'est important pour nous de le souligner afin d'attirer encore plus de femmes dans le domaine de l'informatique.

# Notre solution
Une équipe composée à 100% de femmes.

![](https://media.giphy.com/media/xTiTnLRQtRkaRKkKWs/giphy.gif)

# Le thème
![](img/CS20_logo.png){height=600}

Organisé par l'ETS

## Notre thème ...

## Les suffragettes!
![](img/suffragettes.jpg)

# Horaire
À quoi s'attendre

## Vendredi
+ 19h-22h : Épreuves #1
+ Présentation des flashouts
+ Party

## Samedi
+ 9h-12h : Épreuves #2
+ Dîner
+ 14h-17h : Épreuves #3
+ Souper
+ 19h-22h : Épreuves #4
+ Party

## Dimanche
+ 9h-12h : Épreuves #5
+ Banquet et remises de prix
+ Party de fermeture

# Objectif
## Gagner des petits prix
![](photo-cs2019/petits.jpg)

## ... des petits trophées
![](photo-cs2019/gros.jpg)

## ... et un gros
![](photo-cs2019/win_ets.jpg)

## mais surtout
pour encourager la participation des femmes aux compétitions.

![](https://live.staticflickr.com/5174/5521102662_30207bfffd_b.jpg)


# Qualifications
Ouverture le 8 janvier 00:00:02

Fermeture le 22 janvier 23:59:58

## Prochain rendez-vous
Mercredi 11 décembre

Conférence sur le syndrôme de l'imposteur

![](https://acm-ws.info.uqam.ca/wp-content/uploads/2017/10/acm3.png)

## En attendant
Slack : #competitions 

@corinnep et @Mélanie

